import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State, MATCH, ALL
import requests
import json
import numpy as np
from numpy import argsort
from nltk.tokenize import sent_tokenize
import pandas as pd
import spacy
from operator import itemgetter
from plotly.colors import *
from ast import literal_eval
import flask_monitoringdashboard as dashboard
import logging
from flask import Flask
import plotly.express as px
from datetime import datetime
import os
from pathlib import Path


###################################
## UNALTERED ARCHIMEDES FUNCTIONS
###################################

def pub_dict_pubtator(res):
    """ Generated list of dictionaries for publications and bioconcepts from pubtator """
    # Iterate over publications
    pub_list = []
    for p in res:
        pub_di = {}
        if 'pmid' in p:

            # ### Id
            # pmid
            pub_di['id']={
                'pub_id': p['pmid'],
                'pub_id_type': 'pmid'
            }
            # ### Header
            pub_di['header']={}
            # year
            if 'year' in p['passages'][0]['infons']:
                pub_di['header']['pub_year'] = p['passages'][0]['infons']['year']
            # authors
            if 'authors' in p:
                pub_di['header']['authors'] = p['authors']

            # title
            if ('type' in p['passages'][0]['infons']
                    and  p['passages'][0]['infons']['type'] == 'title'
                    and 'text' in p['passages'][0]):
                pub_di['header']['title'] = p['passages'][0]['text']

            # journal
            if 'journal' in p:
                pub_di['header']['journal'] = p['journal']

                # Journal info (includes doi)
            if 'journal_info' in p['passages'][0]['infons']:
                pub_di['header']['journal_info'] = p['passages'][0]['infons']['journal']

            # bioconcepts in title
            di_title = create_spacy_benchmark_data_from_pubtator_biocjson([p], parts=['title'])
            #print(f'Keys: {di_title}')
            if len(di_title):
                del di_title[pub_di['header']['title']]['pmid']
                pub_di['header']['bioconcepts'] = di_title[pub_di['header']['title']]
            # ### Abstract
            pub_di['abstract'] = {}
            di_abstract = create_spacy_benchmark_data_from_pubtator_biocjson([p], parts=['abstract'])
            di_sent = sentence_tokenize_benchmark_data(di_abstract, keep_sent_wo_bioc=True)
            # +++ b
            pub_di['abstract']
            # +++ e
            pub_di['abstract']={'sentences': list(di_sent.keys()),
                                'bioconcepts': {j:{kk:di_sent[k][kk] for kk in di_sent[k]
                                                   if kk!='pub_part' and kk!='pmid'}
                                                for j, k in enumerate(di_sent.keys())},
                                'labels': [i/len(di_sent.keys()) for i in range(1,len(di_sent.keys())+1)]}

        pub_list.append(pub_di)
    return pub_list#, check_list

def info_from_pubtator_api(pmid_list,
                           Bioconcept=""
                           ):
    """ Get info from pubtator API for list of PMIDs """
    # pmid_list to dict
    jso = {"pmids": [str(p).strip() for p in pmid_list]}

    # load bioconcepts
    if Bioconcept != "":
        jso["concepts"]=Bioconcept.split(",")

    # send request request to pubtator api
    formt='biocjson'
    r = requests.post(
        "https://www.ncbi.nlm.nih.gov/research/pubtator-api/publications/export/"+formt,
        json = jso)
    if r.status_code != 200 :
        print ("[Error]: HTTP code "+ str(r.status_code))
        res=[]
    else:
        # res = '['+r.text.replace("}\n{","},{")+']'
        res = [json.loads(s) for s in r.text.split("\n") if len(s)]
        # +++ b
        # +++ e
    return res

def sentence_tokenize_benchmark_data(di, keep_sent_wo_bioc = False, tokenizer=sent_tokenize):
    """ Tokenizes sentences in abstract. Returns dictionary with sentences as keys
    and entities labeled with pubtator bioconcept classes """
    di_sent = {}
    # Iterate over text keys
    for k in di:
        # Tokenize sentences
        sentences_tmp = sent_tokenize(k)
        # Iterate over the sentences
        for i, s in enumerate(sentences_tmp):
            idx = k.index(s)
            ent_vec = di[k]['entities']
            nam_vec = di[k]['bioconcept']
            id_vec = di[k]['bioconcept_identifier']
            # Entities and names of the sentence
            v = []
            n = []
            id_v = []
            for j, e in enumerate(ent_vec):
                if e[0]-idx >= 0 and e[1]-idx < len(s):
                    v.append((e[0]-idx, e[1]-idx, e[2]))
                    n.append(nam_vec[j])
                    id_v.append(id_vec[j])
                    if s[v[-1][0]:v[-1][1]]!=n[-1]:
                        print(f"Names not equal! {s[v[-1][0]:v[-1][1]]} not {n[-1]}.")
            if len(v) or keep_sent_wo_bioc:
                # Copy the other fields
                di_sent[s]={kk:d for kk, d in di[k].items()
                            if kk!='entities'
                            and kk!='bioconcept'
                            and kk!='bioconcept_identifier'}
                # Entities, names, part + sentence number
                di_sent[s]['pub_part']=di_sent[s]['pub_part']+'_'+str(i)
                if len(v):
                    di_sent[s]['entities']=v
                    di_sent[s]['bioconcept']=n
                    di_sent[s]['bioconcept_identifier']=id_v
    return di_sent


def create_spacy_benchmark_data_from_pubtator_biocjson(res, parts=['title','abstract'], replace_empty_identifiers=True):
    """ Extract annotated data from pubtator biocson list.
    Format: {"Hans lived in Paris": {"entities": [(0, 5, "PERSON"), (14, 19, "LOC")]}
    Can include title and abstract, title only, or abstract only.
    parts = ['title'] title only
    parts = ['abstract'] abstract only
    parts = ['title','abstract'] title and abstract (default)
    """
    di = {}
    if 'title' in parts and 'abstract' in parts:
        v_parts = [0,1]
        parts = ['title','abstract']
    elif 'title' in parts:
        v_parts = [0]
        parts = ['title']
    elif 'abstract' in parts:
        v_parts = [1]
        parts = ['abstract']
    else:
        raise Exception(f"Invalid input value parts={parts}")
    for r in res:
        if 'passages' in r:
            li = r['passages']
            if 'pmid' in r:
                pmid = r['pmid']
            else:
                pmid = 0
            for i in v_parts:
                offset = li[i]['offset']
                if 'annotations' in li[i]:
                    # part (title or abstract)
                    part = li[i]['infons']['type']
                    # annotations
                    ann = li[i]['annotations'] # List of dictionaries
                    # +++ b
                    # print(f"ann={ann}")
                    # +++ e
                    # locations
                    # List of list of dictionaries
                    # e.g. [{'offset': 0, 'length': 8}]
                    if len(ann):
                        en=[]
                        l1=[]
                        # Iterate over annotations
                        for a in ann:
                            loc = a['locations']
                            # +++ b
                            # print(f"loc={loc}")
                            # +++ e
                            # Iterate over locations
                            l1.extend([l['offset'] for l in loc])
                            en.extend([
                                (l['offset']-offset,l['offset']+l['length']-offset,
                                 a['infons']['type'],
                                 a['infons']['identifier'])
                                for l in loc])
                        # Order entities by location
                        ix = argsort(l1)
                        en_ordered = [en[i] for i in ix]
                        # Remove duplicates
                        en_unique=[]
                        id_unique=[]
                        for e in en_ordered:
                            if not e in en_unique:
                                en_unique.append(e[:3])
                                id_unique.append(e[3])
                        # Create output dictionary
                        cur_key = li[i]['text']
                        # Entities and identifiers
                        di[cur_key]={'entities': en_unique, 'bioconcept_identifier': id_unique}
                        # names of entities
                        name_vec = [cur_key[nen[0]:nen[1]] for nen in di[cur_key]['entities']]
                        di[cur_key]['bioconcept'] = name_vec
                        # replace empty identifiers
                        if replace_empty_identifiers:
                            # +++ b
                            # print(f"BEFORE: Identifiers (e.g. MeSH): {di[cur_key]['bioconcept_identifier']}")
                            # +++ e

                            di[cur_key]['bioconcept_identifier']=['NO_ID__'+name_vec[i] if e=='-' or e=='' or e is None or e is np.nan
                                                                  else e
                                                                  for i, e in enumerate(di[cur_key]['bioconcept_identifier'])]
                            # +++ b
                            # print(f"AFTER: Identifiers (e.g. MeSH): {di[cur_key]['bioconcept_identifier']}")
                            # +++ e
                        # part (title or abstract)
                        di[cur_key]['pub_part']=part
                        # pmid
                        di[cur_key]['pmid']=pmid
                    # +++ b todo
                    # Add abstract
                    # +++ e todo
    return di

##########################################
## NEW FUNCTIONS TO CREATE NEW DICTIONARY
##########################################
def get_pubrep_and_clusters():
    '''Uses the example sentence cluster result file and returns three things:
    1. the publication representation for all sentence PMIDs in the file, and
    2. A list of lists of tuples representative of each sentence cluster, [[(pmid_sent1, sentnum_sent1), (pmid_sent2, sentnum_sent2),...][(),()...]]
    3. A list of all sentences present in the file (search)'''

    example_cluster_fname ='configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv'
    with open(example_cluster_fname, 'r') as f:
        df = pd.read_csv(f)

    pmid_list = []
    [pmid_list.append(str(pmid)) for pmid in df['PMID (Link)'] if pmid not in pmid_list] #create list of pmids to create pub rep

    pub_list = info_from_pubtator_api(pmid_list) #create pub rep
    pub_rep = pub_dict_pubtator(pub_list)

    cluster_list=[]
    cluster_sublist=[]
    counter = 0
    k=0 #sentence number
    for i in df.Sentence:
        if df.DistanceToCentral[counter] == 0:
            k=0
            cluster_list.append(cluster_sublist)
            cluster_sublist=[]
            cluster_sublist.append((str(df.pmid[counter]), k))
        else:
            k+=1
            cluster_sublist.append((str(df.pmid[counter]), k))
        counter = counter + 1

    cluster_list.pop(0)
    cluster_list.append(cluster_sublist)
    return pub_rep, cluster_list

def create_cluster_dict(pub_rep, cluster_list):
    '''Creates a nested list of bioconcepts appearing in each cluster [[conc1cluster1, conc2cluster1,....],[conc1cluster2, conc2cluster2,...]...]
    Takes in the publication representation and cluster list. Creates the concept list of concept IDs and a list of concept strings occurring in each cluster.
    Calls calc_percentages to calculate the percentage a bioconcept occurs in each cluster as well as the percentage it occurs
    over all clusters. Calls DepTree with the bioconcept list as an argument to generate a list of lists of parts of speech per
    sentence cluster. Returns a dictionary of cluster_list, words, perc_occ_in_cluster, perc_occ_in_clusters, and pos.'''
    bioconcept_list = [] # list of all concepts occurring in the clusters
    conc_list = [] # list for concept string occurring in all clusters
    for cluster in cluster_list:
        bioconcept_sublist = []
        conc_str_sublist = []
        for tup in cluster: # indiv tuples
            for entry in pub_rep:
                if str(entry['id']['pub_id']) == tup[0]: #pmids are a match, get bioconcepts
                    try:
                        bioconcept_sublist = bioconcept_sublist + entry['abstract']['bioconcepts'][tup[1]]['bioconcept_identifier']
                        conc_str_sublist = conc_str_sublist + entry['abstract']['bioconcepts'][tup[1]]['bioconcept']
                    except KeyError:
                        continue
        conc_list.append(conc_str_sublist) #list of lists of concept strings
        bioconcept_list.append(bioconcept_sublist) #list of lists of concepts

    perc_per_cluster, perc_per_all= calc_percentages(cluster_list, bioconcept_list) # get percentage per cluster and percentage for all clusters
    percpercluster_list = []
    for entry in perc_per_cluster: #get top 10 words per cluster and add to list
        percpercluster_list.append(list(sorted(entry.items(), key=itemgetter(1), reverse=True)[:10]))

    di = {'clusters': cluster_list,
          'words': [],
          'conc_id': [],
          'perc_occ_in_cluster': [],
          'perc_occ_in_all': [],
          'pos': [],
          'entities':[],
          'perc_in_all_by_cluster':[]
          }


    conc_dict = create_concdict(pub_rep) #dictionary {bioconcept text: id}.. traverse dict with typ[0] and get the string attached to it
    entity_dict = create_type_dict(pub_rep, conc_dict)
    word_list = [] #list for concept strings
    entity_list = []
    new_percall_list = [] #this is for the percentages in all clusters by cluster
    for entry in percpercluster_list:
        perc_occ = [] # store percent occurrences
        word_sublist = [] # store concept strings
        entity_sublist = [] #store entity strings (Gene, Chemical, etc)
        new_percall_sublist=[]
        for tup in entry:
            perc_occ.append(tup[1]) #tup[1] = percent occurs
            #id.append(tup[0]) #tup[0] is concept id
            if tup[0] in perc_per_all.keys():
                di['perc_occ_in_all'].append(perc_per_all[tup[0]]) #append the value associated with the key
            for k, v in conc_dict.items(): # create a nested list of concept strings
                if tup[0] == v:
                    word_sublist.append(k) # add the string to the list
            for k, v in entity_dict.items(): #append the entity string associated with the bioconcepts in the clusters
                if tup[0] == k:
                    entity_sublist.append(v)
                    continue
            if tup[0] in perc_per_all.keys():
                new_percall_sublist.append(perc_per_all[tup[0]])

        word_list.append(word_sublist) # add to big list
        entity_list.append(entity_sublist)
        new_percall_list.append(new_percall_sublist)

        di['perc_occ_in_cluster'].append(perc_occ)
        di['words']=word_list
        di['entities']=entity_list
        di['perc_in_all_by_cluster'] = new_percall_list

    flat_list = [item for sublist in word_list for item in sublist] #flatten the nested list of concept strings
    for l in word_list:
        sub = []
        for c in l:
            sub.append('PROPN')
        di['pos'].append(sub)
    # pos_list = DepTree_wo_pmid(flat_list) #feed to nlp
    # for d in pos_list:
    #  for v in d.values():
    #   di['pos']+= v #add to dictionary
    return di

def create_concdict(pub_rep):
    '''creates and returns a dictionary with bioconcept id's as keys and bioconcept strings as values. Takes in the publication representation.'''
    conc_di = {}
    for entry in pub_rep:
        for k in entry['abstract']['bioconcepts']:
            try:
                for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                    conc_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]] = entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]
            except KeyError:
                continue
    new_di = {} # intermediate dictionary to remove duplicate values and keep only the one with shortest length
    for key, val in conc_di.items():
        new_di.setdefault(val, set()).add(key)

    for key, val in new_di.items():
        if len(val) > 1: #duplicate values exist
            temp_list = list(val) # turn sets to lists
            new_di[key] = min(temp_list, key=len) # find the minumum and replace the values with it
        else:
            new_di[key] = ''.join(val) # if length was 1, turn the set to a string
    conc_dict = {value: key for (key, value) in new_di.items()} # invert the dictionary

    return conc_dict

#sentence cluster file does not contain all sentences in the pub rep... does it take into account titles??

def DepTree_wo_pmid(sentences):
    '''Takes in a list of sentences and identifies the parts of speech present. Returns a list of dictionaries of parts of speech,
    [{'pos: 'NOUN', 'ADJ',....},{'pos': 'VERB', 'PROPN',...}]'''
    model = "en_core_web_sm"
    # Load spacy model
    nlp = spacy.load(model)
    li = []
    i = 0
    for s in sentences:
        di = dict()
        d = nlp(s)
        i+=1
        djo = d.to_json()
        for t in d:
            di['pos']=[]
            r = 0
        for t in djo['tokens']:
            di['pos'].append(t['pos'])
        li.append(di)
    return li

def calc_percentages(cluster_list, bioconcept_list):
    '''calculates the percentage that a bioconcept appears in an individual sentence cluster. Takes the cluster list and bioconcept lists as input and returns
    a list of dictionaries with bioconcept identifiers as keys and percentage of appearance as values. Each dictionary corresponds to one cluster.'''
    percpercluster_list = [] #list to hold percentages referring to individual sentence clusters
    k=0
    for conc in bioconcept_list: #calculate percent of time each bioconcept occurs per cluster
        count_di = {i:round(conc.count(i)/len(cluster_list[k])*100) for i in conc} #/len(conc) or over len(cluster_list)?
        percpercluster_list.append(count_di)
        k+=1

    percall_new_dict = {} #{bioconcept id: num clusters occurs in}

    big_list = []
    for entry in bioconcept_list: #eliminate duplicates and count num occ
        big_list.append(list(set(entry)))

    for j in range(len(big_list)): #for each list, traverse and make key if not in list and assign value to 1, then continue to next entry
        for i in range(len(big_list[j])):
            target=big_list[j][i] #this is the concept to insert into dictionary
            if target in percall_new_dict.keys():
                percall_new_dict[big_list[j][i]]+=1
            else:
                percall_new_dict[big_list[j][i]]=1
    for k, v in percall_new_dict.items():
        percall_new_dict[k] = round(100-(v/len(cluster_list)*100))
    return percpercluster_list, percall_new_dict#, new_percall_list

def create_type_dict(pub_rep, conc_dict):
    '''creates and returns a dictionary with bioconcept strings as keys and bioconcept entity type as values. Takes in the publication representation and the
    dictionary with bioconcept ids as values and bioconcept strings as keys.'''
    entity_di={}
    for key in conc_dict.keys():
        target = key #target bioconcept string
        for entry in pub_rep:
            for k in entry['abstract']['bioconcepts']:
                try:
                    for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                        if entry['abstract']['bioconcepts'][k]['bioconcept'][j]==target: #if concepts match
                            entity_di[entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]]=entry['abstract']['bioconcepts'][k]['entities'][j][2]
                except KeyError:
                    #logger.warning('no key here')
                    continue
    return entity_di

def get_sentences_in_clusters(df):
    '''takes a pandas DataFrame.
    Returns a list of central sentences, and a list of lists of child sentences
    each sentence with their respective date and PMID attached to them.'''
    pa = []
    ch = []
    tmp = []
    counter = 0
    for i in df.Sentence:
        if df.DistanceToCentral[counter] == 0:
            pa.append(i + ' ' + str(df.Date[counter]) + ' PMID: ' + str(df.pmid[counter]))
            ch.append(tmp)
            tmp =[]
        else:
            tmp.append(i + ': ' + str(df.Date[counter]) + ' PMID: ' + str(df.pmid[counter]))
        counter = counter + 1
    ch.pop(0)
    ch.append(tmp)
    return pa, ch


################################
## LAYOUT FUNCTIONS
################################

def get_background_color(term):
    '''Defines the background color for the colored panels. Takes in a term (concept/percentage) and returns a color after calling get_color_for_val.'''
    color_list = px.colors.sequential.Turbo
    rev_color_list = [c for c in reversed(color_list)]
    if type(term) == int: #if percentage
        pl_colors =  px.colors.sequential.Rainbow[::-1]
        color_vals=np.array(list(range(0, 100)))
        vmin= color_vals.min()
        vmax=color_vals.max()
        bgcolor = [get_color_for_val(v, vmin,vmax,pl_colors) for v in color_vals]
        return bgcolor[term-1]
    if type(term) == str: #concept/verb
        if term == 'Gene':
            return 'purple'
        elif term == 'Disease':
            return 'orange'
        elif term == 'Chemical':
            return 'green'
        elif term == 'Mutation':
            return 'brown'
        elif term == 'Species':
            return 'lightblue'
        else: #term is CellLine
            return 'turquoise'

def get_color_for_val(val, vmin, vmax, pl_colors):
    '''maps a percentage to an rgb color. Returns a color.'''
    if pl_colors[0][:3] != 'rgb':
        raise ValueError('This function works only with Plotly  rgb-colorscales')
    if vmin >= vmax:
        raise ValueError('vmin should be < vmax')

    scale = [k/(len(pl_colors)-1) for k in range(len(pl_colors))]
    colors_01 = np.array([literal_eval(color[3:]) for color in pl_colors])/255.  #color codes in [0,1]
    v= (val - vmin) / (vmax - vmin) # val is mapped to v in [0,1]
    #find two consecutive values in plotly_scale such that   v is in  the corresponding interval
    idx = 1
    while(v > scale[idx]): #sequential searching of the interval
        idx += 1
    vv = (v - scale[idx-1]) / (scale[idx] -scale[idx-1] )

    #get   [0,1]-valued color code representing the rgb color corresponding to val
    val_color01 = colors_01[idx-1] + vv * (colors_01[idx ] - colors_01[idx-1])
    val_color_0255 = (255*val_color01+0.5).astype(int)
    return f'rgb{tuple(val_color_0255)}'

def get_text_color(term):
    '''Defines the color of the text in the colored panels.'''
    if term>65:
        return 'white'
    else:
        return 'black'

def make_word_lg(word_list, entity_list, perc_cluster_list, per_all_list, k):
    '''Creates the colored panels containing the concept string, relative percentage, and overall percentage the string occurs. Takes in a list of bioconcepts,
    entities, percentage lists, and a number to ensure a unique ID for the elements, and returns listgroups of colored panels.'''
    ls = []
    for i in range(5):
        lg_group = dbc.ListGroup([
                            dbc.ListGroupItem(
                                    [
                                        html.H6(word_list[i], className="card-title", id='bioconcept-box'+str(i)+str(k)),
                                        dbc.Tooltip("Bioconcept: " +f'{word_list[i]} ' + 'Type: ' + f'{entity_list[i]}', target='bioconcept-box'+str(i)+str(k)),
                                    ],
                            color=get_background_color(entity_list[i]), style={"height": "75px", "scrollbar-width": "thin", "overflow-y": "scroll", "direction":"rtl"}),
                        dbc.ListGroupItem(
                                [
                                    html.H6(str(perc_cluster_list[i])+'%', className="card-title", id='r-box'+str(i)+str(k), style={"color": get_text_color(perc_cluster_list[i])}),
                                    dbc.Tooltip("Percentage " + f'{word_list[i]}' + ' occurs in sentence cluster ' + f'{k}', target = 'r-box'+str(i)+str(k))
                                ],
                    color = get_background_color(perc_cluster_list[i])),
                        dbc.ListGroupItem(
                                [
                                    html.H6(str(per_all_list[i])+'%', className="card-title", id='s-box'+str(i)+str(k), style = {"color": get_text_color(per_all_list[i])}),
                                    dbc.Tooltip("Percentage " + f'{word_list[i]}' + ' occurs in all sentence clusters', target = 's-box'+str(i)+str(k))
                                ],
                    color = get_background_color(per_all_list[i])),
                ],horizontal=True, style={"outline-style": "solid", "outline-color": "black", "margin-right": "10px", "margin-top": "10px", "margin-bottom": "10px"})
        ls.append(lg_group)
    return ls

def make_wordcards(word_list, entity_list, perc_cluster_list, per_all_list, pa, ch, i, cluster_length):
    '''creates the layout for the colored panels, checkbox/toggle button, and sentence clusters. Takes in a list of concepts, entities, percentage lists,
    parent sentence list, child sentence list, and a number and returns the layout.'''
    cards = dbc.CardGroup([
        dbc.ListGroup(
                    [dbc.Row([
                        dbc.Col(
                            dbc.Card(
                            dbc.CardBody(
                             [
                                 dbc.Row(
                                     [
                                        dcc.Checklist(
                                            id = {'type': 'cl_central', 'index': i, 'type_id':'_'+str(i)+' _sentence'}, options=[{'label': '', 'value': pa},],value=[pa],
                                        ),
                                        dbc.Button(
                                            f"{i}. ({len(ch)+1})  ",
                                            color="info",
                                            outline=True,
                                            id={'type': 'sentence', 'index': i}
                                        ),
                                    ]),
                            ]),
                                ),width={"size":.5, "offset":1}, style={"margin-top": "10px"},
                        ),

                    dbc.Col(
                     dbc.ListGroup(make_word_lg(word_list, entity_list, perc_cluster_list, per_all_list, i), horizontal=True,),#style={"outline-style": "solid", "outline-color": "black"}),
                    xs=3),
                ],className="g-0",),
                ], horizontal=True),
        dbc.Row(
            dbc.Col(dbc.Card(make_string_item(pa, ch, i)), style={"margin-left":"10px"}, xs=12)) #width={"offset":1}
    ])
    return cards


def make_string_item(pa, ch, i):
    '''Takes a list of central sentences, a list of lists of child sentences, and an integer
	Returns Dash Card and Collapse objects that displays sentences'''
    layout = dbc.Card(
        [
            dbc.CardHeader(
                dbc.Row([
                    #dbc.Button(
                       # f"{i}. ({len(ch)+1})  ",
                       # color="info",
                       # outline=True,
                      #  id={'type': 'sentence', 'index': i}
                        # id=f"group-{i}-toggle",
                   # ),
                    html.Div([ #was H6
                        dcc.Checklist(id = {'type': 'cl_clild', 'index': i, 'type_id':'_'+str(i)+' _sentence'},options=[{'label': f" {pa}", 'value': pa},], value=[pa]),
                        make_cl(pa),])
                ]
                )
            ),
            dbc.Collapse(
                make_lgt1(ch, i, append_str='_'+str(i)),
                # make_lgt(ch),
                id={'type': 'sent_collapse', 'index': i}
                # id=f"collapse-{i}",
            ),
        ]
    )
    return layout

def make_lgt1(ch, i, append_str=''):
    '''makes a dash 'list group item' containing checklists. Takes a list as an argument uses each component
       of the list as a separate list group item and checklist as well as link to pubtator. 'unique_num' used
       to give every Checklist a unique 'index', which is needed for the callback'''
    ls = []

    for x in range(len(ch)): #number of sentences in abstract\
        unique_num = int(str(i) + str(x))
        ls.append(dbc.ListGroup([ #index was unique_num
            dbc.ListGroupItem(dcc.Checklist(id={'type': 'cl_clild', 'index': unique_num, 'type_id':append_str +' _sentence'}, options=[{'label': f" {x + 1}. {ch[x]}", 'value': ch[x]},], value=[ch[x]])),
            dbc.ListGroupItem("View on PubTator >>> ", href="https://www.ncbi.nlm.nih.gov/research/pubtator/?view=docsum&query=" + ch[x].split('PMID: ')[1])
        ], horizontal=True,
        ))
    return ls

def make_cl(pa):
    '''Takes a string (a sentence with a labeled PMID) as an argument, and returns a CardLink dash object with
	with that PMID's PubTator page linked'''
    x = dbc.CardLink("View on PubTator >>> ", href="https://www.ncbi.nlm.nih.gov/research/pubtator/?view=docsum&query=" + pa.split('PMID: ')[1])
    return x



with open('configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv', 'r') as f:
    df=pd.read_csv(f)
pa, ch = get_sentences_in_clusters(df)

card_vec=[]
vec = []

pub_rep, cluster_list = get_pubrep_and_clusters()
char_di = create_cluster_dict(pub_rep, cluster_list)

for m in range(int(len(pa))):
    print(char_di['clusters'][m])
    print(len(char_di['clusters'][m]))
    card_vec.append(make_wordcards(char_di['words'][m], char_di['entities'][m], char_di['perc_occ_in_cluster'][m], char_di['perc_in_all_by_cluster'][m], pa[m], ch[m], m+1, len(char_di['clusters'][m])))#need this to be one concept, percentage per entry

server = Flask(__name__)
dashboard.config.init_from(file='/config.cfg')
dashboard.bind(server)
app = dash.Dash(__name__, external_stylesheets = [dbc.themes.BOOTSTRAP], server=server, routes_pathname_prefix='/')#url_base_pathname='/dash/') #this is IMPERATIVE
app.layout = html.Div([
    html.Div(id='word-cards', children=card_vec),
    html.Div(html.H5(dcc.Textarea(id='ouf',
                                  value='Output file:',
                                  style={'margin-left': '15%', 'width': '7%', 'height': 30})),),
    html.Div(html.H5(dcc.Textarea(id ='dummydiv', value = '')))
])

###########################################
## COMPUTATION FUNCTIONS FROM ARCHIMEDES
###########################################
# Select all checkboxes (bioconcepts/topics/sentence clusters)
def select_all(value, options):#, list_options):
    '''Selects all children checkboxes. Takes 'value' (dictates whether checked or not) and
    options, the information associated with a checkbox.
    Returns an empty list if the value of the parent is unchecked ([[]]), or a
    list of all children checkboxes it the value of the parent checkbox is checked.'''

    empty_list = []
    empty_value = [] #indicates uncheck

    if value == [[]]: #if parent checkbox is unchecked, return all empty to uncheck all
        for i in range(len(options)):
            empty_list.append(empty_value)
        return empty_list #all children checkboxes now have [] value, i.e. uncheck all

    new_list = []
    for entry in options: #get options out of nested lists
        for dictionary in entry:
            new_list.append(dictionary)

    to_check = [varY['value'] for varY in new_list if value] #all children checkboxes to check

    new_values = []
    for entry in to_check: #make the value returned a nested list of values
        tmp = []
        tmp.append(entry)
        new_values.append(tmp)
    return new_values #return IDs of all children checkboxes




######################################
## SENTENCE CALLBACKS FROM ARCHIMEDES
######################################

#sentence cluster accordion
@app.callback(
    [Output({'type': "sent_collapse", 'index': MATCH}, "is_open")],
    [Input({'type': "sentence", 'index': ALL}, "n_clicks")],
    [State({'type': "sent_collapse", 'index': ALL}, "is_open")],
    [State('ouf', 'value')], ##DUMMY DIV FOR USER INTERACTION FILES ADDED
    prevent_initial_call=True)
def toggle_accordion_one_only(n_click_v, is_open_v, log_path):
    print(f"I am in toggle_accordion (sentence clusters) with variable number of elements! "
          f"Input: n_click_v: {n_click_v}, is_open_v: {is_open_v}", flush=True)
    ctx = dash.callback_context

    triggered=ctx.triggered #log to file
    log_stuff(triggered, log_path) #### LOG STUFF ADDED

    if not ctx.triggered:
        return [False]*len(n_click_v)
    else:
        button_id = ctx.triggered[0]["prop_id"].split(".")[0]

    print(f"button_id = {button_id}. type(button_id) = {type(button_id)}", flush=True)
    button_id = json.loads(button_id)
    r_v = False
    for i, v in enumerate(n_click_v):
        if button_id['index']==i+1:
            # r_v[i] =  not is_open_v[i]
            r_v =  not is_open_v[i]
    return [r_v]

#select all sentence children
@app.callback(Output({'type':'cl_clild', 'index':ALL, 'type_id':MATCH}, 'value'),
              [Input({'type': 'cl_central', 'index': ALL, 'type_id':MATCH}, 'value')],
              [State({'type':'cl_clild', 'index': ALL, 'type_id':MATCH}, 'options')], #were all match except bioconcept
              State('ouf', 'value'), ## DUMMY DIV FOR USER INTERACTION FILE PATH ADDED
              prevent_initial_call=True,
              suppress_callback_exceptions = True)
def selAllSentenceChildren(value, options, log_path):
    '''Selects all/deselects all sentence cluster children.
    Takes in the value of the sentence parent checkbox and the options of the sentence child
    and returns the desired value of all associated sentence children (checks/unchecks)'''
    ctx = dash.callback_context
    triggered=ctx.triggered
    log_stuff(triggered, log_path) ## LOG STUFF ADDED
    return(select_all(value, options))


##################################
## USER BEHAVIOR/ERROR FUNCTIONS
##################################



@server.route("/")
def my_dash_app():
    '''Flask Monitoring Dashboard'''
    return app.index()

@app.callback(
     Output(component_id='ouf', component_property='value'),
     Input('dummydiv', 'value'))
def get_logfile_path(v):
    '''creates the directory and file path for the yser actions log. Currently writes path to dummy div so it can be accessed by other callbacks.'''
    log_dir = username
    #create user directory
    if not(os.path.exists(log_dir)):
        os.mkdir(log_dir)
    now = datetime.now()
    str_time = now.strftime("%Y_%m_%d_%H_%M_%S")
    log_name = '_'.join((str_time+'_'+session_id+'_user_actions').split(' '))+'.json'
    log_path = str(Path(log_dir)/log_name)
    print('log path ', log_path)
    return log_path

def get_other_paths(log_path, type): #type? error or warning?
    '''returns the file paths of the error and warnings files. Takes in the user actions path as well as a type ('e' or 'w', for example).'''
    stem = Path(log_path).stem
    if type == 'w': #warning
        warning_path = str(Path(log_path).parent)+'/'+str(stem[:-12])+'user_warnings.json'
        return warning_path
    else: #error
        error_path = str(Path(log_path).parent)+'/'+str(stem[:-12])+'user_errors.json'
        return error_path

def log_stuff(triggered, log_path):
    '''log user interactions to file. Takes in the ctx.callback_context dictionary and the file path as input.'''
    print('logging to file: ', triggered)
    current_time = datetime.now()
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    now = datetime.now()
    str_time = now.strftime("%Y_%m_%d_%H_%M_%S")
    logging.basicConfig(filename=log_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

    logger = logging.getLogger(__name__)

    event_info_to_add = {'element': ''.join(triggered[0]['prop_id'].split('.')[0]), #add stuff here
                         'value': triggered[0]['value'], #Double quote to be replaced with \"
                         'time_stamp': str_time}

    with open(log_path, 'r+') as f:
        try:
            before_dict = json.load(f) #load current dictionary in file
            print(before_dict)
            before_dict["user_actions"].append(event_info_to_add)
            f.seek(0)
            json.dump(before_dict, f, indent=4)
        except json.decoder.JSONDecodeError as e:
            actions_dict = {"user_actions":[
                {
                    'element': ''.join(triggered[0]['prop_id'].split('.')[0]),
                    'value': triggered[0]['value'],
                    'timestamp': str_time
                }
            ]}

            json.dump(actions_dict, f)
            error_path = get_other_paths(log_path, 'e')  #get error file path
            print('error path ', error_path)
            with open(error_path, 'w+') as file:
                json.dump({'timestamp': str_time, 'warning': 'warning'}, file) #log error to file




username = 'kirsten123' #sample username to create the user actions directory structure and file paths
session_id = '329058275dfdfg' #sample session ID to create user interaction directory structure and file paths

if __name__ == '__main__':
    app.run_server(debug=True)
